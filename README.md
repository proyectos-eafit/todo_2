# TODO_2 Tutorial

Ok, let's do the second tutorial of a todo list using php and docker but this time using _Laravel_ and _docker-compose_
First of all, we have to define a few things about the tools and the structures we will use to make our web page to vew and store our tasks
I have all the files in this tutorial inside this repo BTW.

## Requirements

### Structure

Ok, we will use two basic elements in our project: users and tasks

_Users:_ I haven't much to say about that. It's just a basic user with password
_Tasks:_ This is our typical structure of task with title and content

![ERD_1](/IMG/ERD_1.png)

_NOTE:_ This is the basic structure of our project and Laravel may add extra data but at this moment, you don't have to worry about it

### Dependencies

Now, we will need a few tools to make our project:

-   [php](https://www.php.net/manual/en/intro-whatis.php)
-   [Composer](https://getcomposer.org/)
-   [Laravel](https://laravel.com/docs/8.x/installation)
-   [Vue.js](https://vuejs.org/)
-   [Docker](https://docs.docker.com/engine/install/ubuntu/)
-   [docker-compose](https://docs.docker.com/compose/)
-   [MariaDB](https://mariadb.org/)
-   [PhpMyAdmin](https://www.phpmyadmin.net/)

You just have to worry about how to install php, docker, docker-compose and Composer; I'll show you how to use MariaDB and PhpMyAdmin with docker and how to install Vue and Laravel with composer but you will need npm and node.js to be able to use Vue.js

## SetUp Laravel, Vue.js and MariaDB

It's time to config our environment before to start the development.

### Laravel and Vue

First, let's open a terminal inside the folder when you want to create your new project and execute these commands to create a new Laravel Project with a basic authentication system:

```bash
composer create-project --prefer-dist laravel/laravel todo
cd todo
composer require laravel/jetstream
```

And execute these commands to configure Vue.js in our project:

```bash
php artisan jetstream:install inertia
npm install
```

And if you want to build your Vue.js code, just execute one of the next commands:

```bash
#if you want to build once
npm run dev
#if you want to build each time you save your code
npm run watch
```

### MariaDB and PhpMyAdmin

Ok, for this, we will use docker and docker-compose to avoid certain kind of issues that we could have if we try to configure this tools on our computers without virtualization. So let's create a file called `docker.compose.yml` inside your project folder with the next content:

```yaml
version: "3.7"
services:
    db:
        image: mariadb:10.5
        restart: always
        environment:
            MYSQL_ROOT_PASSWORD: "toor"
            MYSQL_USER: "todo_user"
            MYSQL_PASSWORD: "todo_password"
            MYSQL_DATABASE: "todo"
        volumes:
            - ./data/var/lib/mysql:/var/lib/mysql
        ports:
            - "127.0.0.1:3306:3306"
        command:
            ["mysqld", "--default-authentication-plugin=mysql_native_password"]

    phpmyadmin:
        image: phpmyadmin:latest
        restart: always
        environment:
            PMA_HOST: db
        ports:
            - "81:80"
```

Now you just have to set the variables inside the environment of mariadb with any values you wish to access the database and use a new terminal to start MariaDB and PhpMyAdmin using the next command in the same folder:

```bash
docker-compose up -d
```

this will create a folder with the name _data_ with all the data of your database. That folder is very important because if you don't have it configured in your `docker-compose.yml`, you will lose all your data if you stop docker-compose.
Also, I'll give you a command to stop docker-compose because, you know, we may (or may not) have to sotp our computers and it's ugly to turn it off with processes on the background:

```bash
docker-compose down
```

## It's time to code (so so)

Now our project should work but not at all because we have to connect Laravel with MariaDB and for that, we have to open the file _.env_ in our project and I will show you just the lines you have to modify:

```env
DB_DATABASE=todo
DB_PASSWORD=todo_password
DB_USERNAME=todo_user
```

And with that, it's time to make a migration that it's a way to say 'hey laravel, please create this structure in the database' but at this moment, we just want to do a migration of the basic structure that laravel need:

```bash
php artisan migrate
```

And now you can go to phpmyadmin, login with the todo user, go to the todo database and see that laravel already did a basic structure of the database
And we already have a good configured User table with user, password, email and other things thanks to the basic project of laravel. I won't configure this user because it has all I need to continue.

### Create the structure and access for Tasks

Let's do this straightforward. Just execute the next commands to create a basic files as starting point to make the structure of the tasks:

```bash
#to create the migration file
php artisan make:migration create_tasks_table

#to create the controller of the Tasks table
php artisan make:model Task
```

Now we have to edit the migration of tasks that in my case will be in the route `database/migrations/2020_10_19_234628_create_tasks_table.php` but remember that it cout change depending on the date you created it. So, we have to open it and it has to have this structure:

```php
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("title");
            $table->text("content");
            $table->unsignedBigInteger('user_id');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('tasks');
    }
}
```

This will make the structure of our tasks in the database and we have to add extra data that laravel requires to manage our data and this will be added automatically to the database when we do a migration. BTW, it's time to do a migration:

```bash
php artisan migrate
```

Now it's time to open the file that represents our Task object that is in the route `app/Models/Task.php` and it should be like this:

```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model{
    use HasFactory;
    protected $fillable = [
        'title', 'content', 'user_id'
    ];
}
```

In the variable `$fillable` we have to put a list of elements of our table that we will be able to insert. The rest of elements of our table will be filled automatically by laravel.
After that, we have to create a route to access our Tasks and to show a web page to the user so, we have to open the file `routes/web.php` and add this code at the end of the file:

```php

Route::middleware(['auth:sanctum', 'verified'])->resource('tasks', TaskController::class);

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return redirect('/task');
})->name('dashboard');
```

Here we did a route to redirect from `/dashboard` that it's the preconfigured route after a login to `/tasks` that it's the place when we will show our content and where we will take data of our tasks
Now we have to create a controller to our tasks at `app/Http/Controllers/TaskController.php` an it has to have the next content:

```php
<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Task;
use Inertia\Inertia;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class TaskController extends Controller{
    /**
     * Returns a list of Tasks
     *
     * @return Response
     */
    public function index(){
        $data = Task::select('id', 'title', 'content')
              ->where("user_id", Auth::id())
              ->get();
        return Inertia::render('Dashboard',['data'=>$data]);
    }

    /**
     * Creates a new Task given a title and a content
     *
     * @return Response
     */
    public function store(Request $request){
        $data = $request->all();
        Validator::make($data, [
            'title' => ['required'],
            'content' => ['required']
        ])->validate();
        $data["user_id"] = Auth::id();
        Task::create($data);
        return redirect()->back()
                         ->with('message', 'Task Created Successfully.');
    }

    /**
     * Updates a specific Task that have a given number
     *
     * @return Response
     */
    public function update(Request $request){
        $request->offsetUnset('_method');
        Validator::make($request->all(), [
            'title' => ['required'],
            'content' => ['required']
        ])->validate();
        if ($request->has('id')) {
            Task::find($request->input('id'))->where('id', $request->input('id'))->where("user_id", Auth::id())->update($request->all());
            return redirect()->back()
                             ->with('message', 'Task Updated Successfully.');
        }
    }

    /**
     * Deletes a given Task.
     *
     * @return Response
     */
    public function destroy(Request $request){
        if ($request->has('id')) {
            Task::where("id", $request->input('id'))
                ->where("user_id", Auth::id())
                ->delete();
            return redirect()->back();
        }
    }

}
```

Ok, this file will manage the way we get and insert information in the database on the way the comments above each function explains and also will give us a Vue page when we acess to `/tasks` using a web browser.

Now we have to do an extra configuration in our service provider to be able to manage Vue.js so we have to open the file `app/Providers/AppServiceProvider.php` and add the next content to the function boot():

```php
public function boot(){
    Inertia::share([
        'errors' => function () {
            return Session::get('errors')
                ? Session::get('errors')->getBag('default')->getMessages()
                : (object) [];
        },
    ]);

    Inertia::share('flash', function () {
        return [
            'message' => Session::get('message'),
        ];
    });
}
```

And also, we have to add this lines before the class:

```php
use Illuminate\Support\Facades\Session;
use Inertia\Inertia;
```

And at this point our backend should be ready so it's time to develop the frontend.

### Frontend with Vue

Ok, laravel already has a few pages (welcome, login and register) using blade so we will take advantage of it so we will only develop our task view using Vue.js.
Something to take in mind when you use Vue.js is that it uses components. It means that you can split your code in different files and import those files to use them as html and usually that is a good practice 'cause it helps you to have clean code but in this case it could be a little bit difficult to split the code because it uses parts that require a lot of calls between components and that makes it a bit difficult to read and to write so i just did a component for this view and I think it's good to start viewing it:

First of all, let's create this file on `./resources/js/Pages/components/taskList.vue`:

```vue
<template>
    <table class="table-fixed w-full">
        <thead>
            <tr class="bg-gray-100">
                <th class="px-4 py-2 w-20">No.</th>
                <th class="px-4 py-2">Title</th>
                <th class="px-4 py-2">Content</th>
                <th class="px-4 py-2">Action</th>
            </tr>
        </thead>
        <tbody>
            <tr v-for="row in tasks" :key="row.id">
                <td class="border px-4 py-2">{{ row.id }}</td>
                <td class="border px-4 py-2">
                    {{ row.title }}
                </td>
                <td class="border px-4 py-2">{{ row.content }}</td>
                <td class="border px-4 py-2">
                    <button
                        @click="edit(row)"
                        class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                    >
                        Edit
                    </button>
                    <button
                        @click="deleteRow(row)"
                        class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded"
                    >
                        Delete
                    </button>
                </td>
            </tr>
        </tbody>
    </table>
</template>

<script>
export default {
    props: ["tasks"],
    methods: {
        deleteRow: function(data) {
            if (!confirm("Are you sure want to remove?")) return;
            data._method = "DELETE";
            this.$inertia.post("/tasks/" + data.id, data);
            this.reset();
            this.$emit("closeModal");
        },
        edit: function(row) {
            this.$emit("edit", row);
        }
    }
};
</script>
```

This file just have a table with a list of tasks and also it has two methods to delete a row and to send a signal to the parent to edit that task. You may be wondering why do I send a signal to the parent instead of editing that task from here? Well, that's because the function to edit tasks uses variables from the parent and if I put that function here, it'd be a mess 'cause of all the signals to manage so it's better to put that function there and send a signal from here.

Now I'll show you the View or in other words, the element that Laravel calls to show a web page. That file is under `./resources/js/Pages/Dashboard.vue` and it has the next content:

```vue
<template>
    <app-layout>
        <template #header>
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                TODO-List
            </h2>
        </template>
        <div class="py-12">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <div
                    class="bg-white overflow-hidden shadow-xl sm:rounded-lg px-4 py-4"
                >
                    <div
                        class="bg-teal-100 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md my-3"
                        role="alert"
                        v-if="$page.flash.message"
                    >
                        <div class="flex">
                            <div>
                                <p class="text-sm">{{ $page.flash.message }}</p>
                            </div>
                        </div>
                    </div>
                    <button
                        @click="openModal()"
                        class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded my-3"
                    >
                        Create New Post
                    </button>
                    <taskList :tasks="data" @edit="edit"></taskList>
                    <div
                        class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400"
                        v-if="isOpen"
                    >
                        <div
                            class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0"
                        >
                            <div class="fixed inset-0 transition-opacity">
                                <div
                                    class="absolute inset-0 bg-gray-500 opacity-75"
                                ></div>
                            </div>

                            <span
                                class="hidden sm:inline-block sm:align-middle sm:h-screen"
                            ></span
                            >​
                            <div
                                class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full"
                                role="dialog"
                                aria-modal="true"
                                aria-labelledby="modal-headline"
                            >
                                <form>
                                    <div
                                        class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4"
                                    >
                                        <div class="">
                                            <div class="mb-4">
                                                <label
                                                    for="exampleFormControlInput1"
                                                    class="block text-gray-700 text-sm font-bold mb-2"
                                                    >Title:</label
                                                >
                                                <input
                                                    type="text"
                                                    class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="exampleFormControlInput1"
                                                    placeholder="Enter Title"
                                                    v-model="form.title"
                                                />
                                                <div
                                                    v-if="$page.errors.title"
                                                    class="text-red-500"
                                                >
                                                    {{ $page.errors.title[0] }}
                                                </div>
                                            </div>
                                            <div class="mb-4">
                                                <label
                                                    for="exampleFormControlInput2"
                                                    class="block text-gray-700 text-sm font-bold mb-2"
                                                    >Content:</label
                                                >
                                                <textarea
                                                    class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                    id="exampleFormControlInput2"
                                                    v-model="form.content"
                                                    placeholder="Enter Content"
                                                ></textarea>
                                                <div
                                                    v-if="$page.errors.content"
                                                    class="text-red-500"
                                                >
                                                    {{
                                                        $page.errors.content[0]
                                                    }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse"
                                    >
                                        <span
                                            class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto"
                                        >
                                            <button
                                                wire:click.prevent="store()"
                                                type="button"
                                                class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-green-600 text-base leading-6 font-medium text-white shadow-sm hover:bg-green-500 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5"
                                                v-show="!editMode"
                                                @click="save(form)"
                                            >
                                                Save
                                            </button>
                                        </span>
                                        <span
                                            class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto"
                                        >
                                            <button
                                                wire:click.prevent="store()"
                                                type="button"
                                                class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-green-600 text-base leading-6 font-medium text-white shadow-sm hover:bg-green-500 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5"
                                                v-show="editMode"
                                                @click="update(form)"
                                            >
                                                Update
                                            </button>
                                        </span>
                                        <span
                                            class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto"
                                        >
                                            <button
                                                @click="closeModal()"
                                                type="button"
                                                class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-medium text-gray-700 shadow-sm hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5"
                                            >
                                                Cancel
                                            </button>
                                        </span>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </app-layout>
</template>
<script>
import AppLayout from "./../Layouts/AppLayout";
import taskList from "./components/taskList";
export default {
    components: {
        AppLayout,
        taskList
    },
    props: ["data", "errors"],
    data() {
        return {
            editMode: false,
            isOpen: false,
            form: {
                title: null,
                content: null
            }
        };
    },
    methods: {
        openModal: function() {
            this.isOpen = true;
        },
        closeModal: function() {
            this.isOpen = false;
            this.reset();
            this.editMode = false;
        },
        reset: function() {
            this.form = {
                title: null,
                content: null
            };
        },
        save: function(data) {
            this.$inertia.post("/tasks", data);
            this.reset();
            this.closeModal();
            this.editMode = false;
        },
        edit: function(data) {
            this.form = Object.assign({}, data);
            this.editMode = true;
            this.openModal();
        },
        update: function(data) {
            data._method = "PUT";
            this.$inertia.post("/tasks/" + data.id, data);
            this.reset();
            this.closeModal();
        },
        deleteRow: function(data) {
            if (!confirm("Are you sure want to remove?")) return;
            data._method = "DELETE";
            this.$inertia.post("/tasks/" + data.id, data);
            this.reset();
            this.closeModal();
        }
    }
};
</script>
```

I know, it's too big but again, the other option is to make a lot of calls between the components and that's a mess. So, this component calls and renders our list of tasks that we made on the previous file and also has a few modals to create and edit our tasks. It also has methods to send data to laravel and to show/hide our modals and do you remember that we edited a file to be able to use Vue? that's because we needed to pass data from Laravel to Vue and we do that use props so we pass data (our list of tasks in this case) and errors that laravel could generate trying to do thing with our data.

## Let's try the results

That's it. Our web page is ready and we just need to try it so we have to execute the next commands:

In case you have stopped docker-compose, execute this command:

```bash
docker-compose up -d
```

To build our Vue.js components, run this:

```bash
npm run dev
```

And finally, to execute laravel, do this:

```bash
php artisan serve
```

And now if you go to `127.0.0.1:8000` using a web browser, you will see a web page like this:

![welcome_view](IMG/welcome_view.png)

And if you press the text at the top-right of the page that says `register` you will be able to register and you will see this:

![register_view](IMG/register_view.png)

After that, you will be able to see the dashboard of your web page:

![dashboard_view](IMG/dashboard_view.png)

And now you can insert, update and delete your tasks from here:

Insert new task
![insert_view](IMG/insert_view.png)

Edit task
![edit_view](IMG/edit_view.png)

And you can delete a task pressing the button `delete` beside each task.
